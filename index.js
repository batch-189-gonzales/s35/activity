const express = require('express');

const mongoose = require('mongoose');

const app = express();

const port = 3001;

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.uzz5mzo.mongodb.net/S35-Activity?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));
db.on('open', () => console.log('Connected to MongoDB!'));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Create User Schema
const UserSchema = new mongoose.Schema({
	username: String,
	password: String
});

//Create User Model
const User = mongoose.model('User', UserSchema);

//Sign-up Route
app.post('/sign-up', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result != null && result.username == req.body.username){
			return res.send(`Username ${req.body.username} is already in use.`);
		} else {
			if(req.body.username != 
			"" && req.body.password != ""){
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});
				newUser.save((error, result) => {
					if(error){
						return res.send(error);
					} else {
						return res.send('New user has been registered!')
					};
				});
			} else {
				return res.send('Please enter BOTH username and password.');
			};
		};
	});
});

app.listen(port, () => console.log(`Server is running at port ${port}`));